let vector = [1, 'dos', true, null, {nombre: 'Juan'}, ['elemento1', 'elemento2']];

console.log(vector);

console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

vector[2] = false;

console.log('Longitud del vector:', vector.length);

vector.push('nuevo elemento');

const elementoEliminado = vector.pop();
console.log('Elemento eliminado:', elementoEliminado);

vector.splice(Math.floor(vector.length / 2), 0, 'elemento intermedio');

const primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

vector.unshift(primerElemento);

console.log('Vector actualizado:', vector);