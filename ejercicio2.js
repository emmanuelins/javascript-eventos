const vector = [1, 'dos', true, 4, 'cinco', false];

console.log('a. Imprimir en la consola usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

console.log('b. Imprimir en la consola usando "forEach":');
vector.forEach(element => {
  console.log(element);
});

console.log('c. Imprimir en la consola usando "map":');
vector.map(element => {
  console.log(element);
});

console.log('d. Imprimir en la consola usando "while":');
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}

console.log('e. Imprimir en la consola usando "for..of":');
for (const element of vector) {
  console.log(element);
}